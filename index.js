const createError = require('http-errors');
const express = require('express');
const app = express();

const cors = require('cors');
const logger = require('morgan');
const path = require('path');
require('dotenv').config();

app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// import routes
const indexRouter = require('./routes');

app.use('/api', indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	res.status(err.status || 500);
	res.json({ message: err.message || 'Something went wrong...' });
});

app.listen(process.env.PORT, () => {
	console.log(`Listening on ${process.env.PORT}`);
});
